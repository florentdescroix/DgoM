#-------------------------------------------------
#
# Project created by QtCreator 2015-05-22T17:21:18
#
#-------------------------------------------------

CONFIG+= static

QT += core xml svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DgoM
TEMPLATE = app

SOURCES += src/main.cpp\
	src/preferences.cpp\
	src/gui/mainwindow.cpp \
	src/game/game.cpp \
    src/game/player.cpp \
    src/game/territory.cpp \
    src/game/score.cpp \
    src/game/stone.cpp \
	src/game/move.cpp \
	src/gui/config/generalconfig.cpp \
	src/gui/graphicsview/gameview.cpp \
	src/gui/graphicsview/gamescene.cpp \
	src/gui/graphicsview/themerenderer.cpp \
	src/gui/widgets/setupwidget.cpp \
	src/gui/widgets/analysiswidget.cpp \
	src/gui/widgets/gamewidget.cpp \
	src/gui/widgets/errorwidget.cpp \
	src/include/kgamepopupitem.cpp

HEADERS  += src/preferences.h\
	src/gui/mainwindow.h \
	src/game/game.h \
    src/game/player.h \
    src/game/territory.h \
    src/game/score.h \
    src/game/stone.h \
	src/game/move.h \
	src/gui/config/generalconfig.h \
	src/gui/graphicsview/gameview.h \
	src/gui/graphicsview/gamescene.h \
	src/gui/graphicsview/themerenderer.h \
	src/gui/widgets/errorwidget.h \
	src/gui/widgets/analysiswidget.h \
	src/gui/widgets/setupwidget.h \
	src/gui/widgets/gamewidget.h \
	src/include/kgamepopupitem.h

FORMS    += src/gui/mainwindow.ui \
	src/gui/config/generalconfig.ui \
	src/gui/mainwindow.ui \
	src/gui/widgets/analysiswidget.ui \
	src/gui/widgets/setupwidget.ui \
	src/gui/widgets/errorwidget.ui \
	src/gui/widgets/gamewidget.ui

TRANSLATIONS = data/languages/Translation_en.ts \
	data/languages/Translation_kr.ts
