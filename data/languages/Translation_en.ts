<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AnalysisWidget</name>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="14"/>
        <source>Analyse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="20"/>
        <source>Player:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="133"/>
        <source>W&amp;hite Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="146"/>
        <source>Black Pla&amp;yer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="158"/>
        <source>Tools:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="167"/>
        <source>Add &amp;multiple pawns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="196"/>
        <source>&amp;Add one pawn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="257"/>
        <source>&amp;Remove pawn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="319"/>
        <source>Launch Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/analysiswidget.ui" line="326"/>
        <source>Leave Analysis Mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorWidget</name>
    <message>
        <location filename="../../src/gui/widgets/errorwidget.ui" line="14"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/errorwidget.ui" line="26"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Oxygen-Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; color:#ff0000;&quot;&gt;DgoM was unable to find a  Go engine backend.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; color:#ff0000;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;;&quot;&gt;If you are sure that you already installed a suitable Go engine, you might want to configure DgoM to use that engine. Otherwise you should install a Go engine (like GnuGo).&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/errorwidget.ui" line="60"/>
        <source>Configure DgoM...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Game</name>
    <message>
        <location filename="../../src/game/game.cpp" line="358"/>
        <source>White %1</source>
        <comment>stone coordinate</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/game/game.cpp" line="361"/>
        <location filename="../../src/game/game.cpp" line="458"/>
        <source>White passed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/game/game.cpp" line="367"/>
        <source>Black %1</source>
        <comment>stone coordinate</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/game/game.cpp" line="370"/>
        <location filename="../../src/game/game.cpp" line="460"/>
        <source>Black passed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/game/game.cpp" line="467"/>
        <source>White resigned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/game/game.cpp" line="469"/>
        <source>Black resigned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/game/game.cpp" line="477"/>
        <source>White %1</source>
        <comment>response from Go engine</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/game/game.cpp" line="479"/>
        <source>Black %1</source>
        <comment>response from Go engine</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GameWidget</name>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="14"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="22"/>
        <source>Handicap:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="38"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="45"/>
        <source>Komi:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="61"/>
        <source>0.5 Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="81"/>
        <source>Last move:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="104"/>
        <source>Move:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="187"/>
        <source>White Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="194"/>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="268"/>
        <source>Captures:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="236"/>
        <source>Black Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.ui" line="308"/>
        <source>Finish Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="54"/>
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="59"/>
        <source>Computer (level %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="62"/>
        <source>%1 Stone(s)</source>
        <translation type="unfinished">
            <numerusform>%1 Stone</numerusform>
            <numerusform>%1 Stones</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="81"/>
        <source>%1 (white)</source>
        <comment>Indication who played the last move</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="83"/>
        <source>%1 (black)</source>
        <comment>Indication who played the last move</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="92"/>
        <source>White&apos;s turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="96"/>
        <source>Black&apos;s turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="101"/>
        <location filename="../../src/gui/widgets/gamewidget.cpp" line="102"/>
        <source>%1 capture(s)</source>
        <translation type="unfinished">
            <numerusform>%1 capture</numerusform>
            <numerusform>%1 captures</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>GeneralConfig</name>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="14"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="24"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="36"/>
        <source>Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="58"/>
        <source>Parameters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="99"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please select a Go engine that supports the &lt;span style=&quot; font-style:italic;&quot;&gt;GnuGo Text Protocol (GTP)&lt;/span&gt;. The indicator light turns green when the selected backend is working.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="119"/>
        <source>Executable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="161"/>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="210"/>
        <source>Display Board Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="230"/>
        <source>&amp;Hint Visibility Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="247"/>
        <location filename="../../src/gui/config/generalconfig.ui" line="297"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="268"/>
        <source>No preview...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="284"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="327"/>
        <source>Contact:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="337"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="361"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="377"/>
        <source>Theme Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="422"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="429"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.ui" line="436"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/config/generalconfig.cpp" line="67"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/gui/config/generalconfig.cpp" line="119"/>
        <source> Second(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="20"/>
        <source>DgoM</source>
        <translation>DgoM (English)</translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="34"/>
        <source>Ga&amp;me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="47"/>
        <source>Mo&amp;ve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="60"/>
        <source>Settin&amp;gs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="64"/>
        <source>&amp;Toolbars Shown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="72"/>
        <source>&amp;Dockers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="82"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="90"/>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="115"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="118"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="128"/>
        <source>&amp;Load...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="131"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="141"/>
        <source>Start &amp;Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="144"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="154"/>
        <source>&amp;Finish Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="157"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="167"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="170"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="180"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="183"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="193"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="196"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="206"/>
        <source>&amp;Pass move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="209"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="214"/>
        <source>&amp;Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="217"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="230"/>
        <source>Show move &amp;number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="233"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="244"/>
        <source>&amp;Main Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="255"/>
        <source>M&amp;ove Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="266"/>
        <source>&amp;Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="277"/>
        <source>&amp;Moves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="285"/>
        <source>&amp;Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="295"/>
        <source>&amp;Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="300"/>
        <source>&amp;DgoM Handbook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="305"/>
        <source>&amp;What&apos;s This?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="315"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="318"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="331"/>
        <source>&amp;Analize Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="334"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.ui" line="345"/>
        <source>&amp;View Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="154"/>
        <source>Current Language changed to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="216"/>
        <source>Set up a new game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="222"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="224"/>
        <location filename="../../src/gui/mainwindow.cpp" line="280"/>
        <source>SGF Files (*.sgf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="236"/>
        <source>Set up a loaded game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="239"/>
        <source>Unable to load game...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="278"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="284"/>
        <source>Game saved...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="286"/>
        <source>Unable to save game.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="338"/>
        <source>Game started...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="370"/>
        <source>%1 won with a score of %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="372"/>
        <source>%1 won with a score of %2 (bounds: %3 and %4).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="396"/>
        <source>%1 got %2 territories and %3 got %4 territories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="403"/>
        <source>Undone move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="412"/>
        <source>Redone move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="447"/>
        <source>Backend was changed, restart necessary...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="502"/>
        <source>White passed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="504"/>
        <source>Black passed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="560"/>
        <source>Game Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="572"/>
        <location filename="../../src/gui/mainwindow.cpp" line="578"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="583"/>
        <location filename="../../src/gui/mainwindow.cpp" line="592"/>
        <source>Moves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="586"/>
        <source>No move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="597"/>
        <source>Analyze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/mainwindow.cpp" line="608"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupWidget</name>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="14"/>
        <source>Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="33"/>
        <source>Board size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="52"/>
        <source>Tiny (&amp;9x9)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="74"/>
        <source>Small (&amp;13x13)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="93"/>
        <source>No&amp;rmal (19x19)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="105"/>
        <source>Custom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="154"/>
        <source>Handicap:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="182"/>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="458"/>
        <source>Komi:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="245"/>
        <source> Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="292"/>
        <source>Event:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="327"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="362"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="394"/>
        <source>Round:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="426"/>
        <source>Score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="484"/>
        <location filename="../../src/gui/widgets/setupwidget.cpp" line="237"/>
        <source>Black to play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="497"/>
        <source>Continue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="513"/>
        <source>Time limit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="561"/>
        <source> of 999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="630"/>
        <source>&amp;Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="665"/>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="859"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="710"/>
        <source>Wea&amp;k</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="729"/>
        <source>St&amp;rong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="827"/>
        <source>C&amp;omputer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="904"/>
        <source>Strong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="923"/>
        <source>Weak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.ui" line="1115"/>
        <source>Start Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/gui/widgets/setupwidget.cpp" line="62"/>
        <source> Stone(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/gui/widgets/setupwidget.cpp" line="167"/>
        <source>%1 minute(s)</source>
        <translation>
            <numerusform>%1 minute</numerusform>
            <numerusform>%1 minutes</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/gui/widgets/setupwidget.cpp" line="170"/>
        <source>%1 hour(s), %2</source>
        <translation>
            <numerusform>%1 hour, %2</numerusform>
            <numerusform>%1 hours, %2</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.cpp" line="192"/>
        <source> of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/setupwidget.cpp" line="235"/>
        <source>White to play</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
