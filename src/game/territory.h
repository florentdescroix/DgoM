/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TERRITORY_H
#define TERRITORY_H

#include "stone.h"
#include "game.h"

#include <QList>
#include <QtCore/qmath.h>

class Stone;

class Territory
{
	public:
		Territory();
		Territory(Game::FinalState color, QList<Stone> stones);

		Game::FinalState color() const	{ return m_color; }
		QList<Stone> stonesList() const { return m_stonesList; }
		Stone center() const			{ return m_center; }
		int value() const				{ return m_value; }

		void setColor(const Game::FinalState &color) { m_color = color; }

		void addStone(Stone stone);
		void removeStone (Stone stone);
		void genCenter();

		bool isBlack ()		const { return m_color == Game::FinalBlackTerritory; }
		bool isWhite ()		const { return m_color == Game::FinalWhiteTerritory; }
		bool isInvalid ()	const { return m_color == Game::FinalStateInvalid; }
		bool isDead ()		const { return m_color == Game::FinalDead; }

	private:
		Game::FinalState m_color;
		QList<Stone> m_stonesList;
		Stone m_center;
		int m_value;
};

#endif // TERRITORY_H
