/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "game.h"
#include "score.h"
#include "territory.h"

#include <QDebug>
#include <QFile>

class UndoCommand : public QUndoCommand
{
	public:

		enum MoveType { Stone, Passed, Resigned, Analysis };

		UndoCommand(Player *player, MoveType moveType, const QString &undoStr)
			: QUndoCommand(undoStr), m_player(player), m_moveType(moveType)
		{}

		Player *player () const { return m_player; }
		MoveType moveType () const { return m_moveType; }

	private:

		Player *m_player;
		MoveType m_moveType;
};

Game::Game(QObject *parent)
	: QObject(parent)
	, m_currentMove(0), m_lastUndoIndex(0), m_currentPlayer(&m_blackPlayer)
    , m_blackPlayer(Player::Black), m_whitePlayer(Player::White)
	, m_komi(4.5), m_boardSize(19), m_fixedHandicap(5), m_consecutivePassMoveNumber(0)
    , m_gameFinished(false), m_inAnalysis(false),  m_haveToPopAnalysis(false)
    , m_analysisMovesList(QList< QList<Move> >()), m_currentAnalysisMovesList(new QList<Move>())
{
	connect(&m_process, SIGNAL(readyRead()), this, SLOT(readyRead()));
	connect(&m_undoStack, SIGNAL(canRedoChanged(bool)), this, SIGNAL(canRedoChanged(bool)));
	connect(&m_undoStack, SIGNAL(canUndoChanged(bool)), this, SIGNAL(canUndoChanged(bool)));
	connect(&m_undoStack, SIGNAL(indexChanged(int)), this, SLOT(undoIndexChanged(int)));
}

Game::~Game()
{
	stop();
}

bool Game::start(const QString &command)
{
	stop();                                   // Close old session if there's one
	m_process.start(command.toLatin1());      // Start new process with provided command
	if (!m_process.waitForStarted()) {        // Blocking wait for process start
		m_response = "Unable to execute command: " + command;
		qDebug() << m_response;
		return false;
	}
	m_engineCommand = command;
	//qDebug() << "Game" << command << "started...";

	// Test if we started a GTP-compatible Go game
	m_process.write("name\n");
	m_process.waitForReadyRead();
	QString response = m_process.readAllStandardOutput();
	if (response.isEmpty() || !response.startsWith(QLatin1String("="))) {
		m_response = "Game did not respond to GTP command \"name\"";
		qDebug() << m_response;
		stop();
		return false;
	} else {
		m_engineName = m_response;
	}
	qDebug() << "Game is a GTP-compatible Go game";

	m_process.write("version\n");
	if (waitResponse()) {
		m_engineVersion = m_response;
	}
	return true;
}

void Game::stop()
{
	if (m_process.isOpen()) {
		m_process.write("quit\n");
		m_process.close();
	}
}

bool Game::init()
{
	if (!isRunning()) {
		return false;
	}

	//qDebug() << "Init game!";

	m_process.write("clear_board\n");
	if (waitResponse()) {
		// The board is wiped empty, start again with black player
		setCurrentPlayer(m_blackPlayer);
		m_fixedHandicap = 0;
		m_consecutivePassMoveNumber = 0;
		m_currentMove = 0;
		m_gameFinished = false;
		m_inAnalysis = false;
		m_movesList.clear();
		m_undoStack.clear();
		m_analysisMovesList.clear();
		emit boardChanged();

		return true;
	} else {
		return false;
	}
}

bool Game::init(const QString &fileName, int moveNumber)
{
	Q_ASSERT(moveNumber >= 0);
	if (!isRunning() || fileName.isEmpty() || !QFile::exists(fileName)) {
		return false;
	}

	// When moveNumber specified, GTP don't get the last spawn
	moveNumber = (moveNumber == 0) ? 0 : moveNumber + 1;

	m_process.write("loadsgf " + fileName.toLatin1() + ' ' + QByteArray::number(moveNumber) + '\n');
	if (waitResponse()) {
		if (m_response.startsWith(QLatin1String("white"))) { // Check which player is current
			setCurrentPlayer(m_whitePlayer);
		} else {
			setCurrentPlayer(m_blackPlayer);
		}

		m_process.write("query_boardsize\n");       // Query board size from game
		if (waitResponse()) {
			m_boardSize = m_response.toInt();
		}
		m_process.write("get_komi\n");              // Query komi from game and store it
		if (waitResponse()) {
			m_komi = m_response.toFloat();
		}
		m_process.write("get_handicap\n");          // Query fixed handicap and store it
		if (waitResponse()) {
			m_fixedHandicap = m_response.toInt();
		}
		//qDebug() << "Loaded komi is" << m_komi << "and handicap is" << m_fixedHandicap;

		m_consecutivePassMoveNumber = 0;
		m_currentMove = moveNumber;
		m_gameFinished = false;
		m_inAnalysis = false;
		m_movesList.clear();
		m_movesList = getHistory();
		m_undoStack.clear();
		m_analysisMovesList.clear();

		emit boardSizeChanged(m_boardSize);
		emit boardChanged();                             // All done, tell the world!
		return true;
	} else {
		return false;
	}
}

QList<Move> Game::getHistory() {
	QList<Move> list;

	m_process.write("move_history\n");
	if (waitResponse() && !m_response.isEmpty()) {
		QList<QString> moves = m_response.split('\n');
		for (int i = moves.count() - 1; i >= 0; i--) {
			QList<QString> stone = moves[i].split(' ');
			if (stone[0] == "black")
				list.append(Move(&m_blackPlayer, Stone(stone[1])));
			else
				list.append(Move(&m_whitePlayer, Stone(stone[1])));
		}
	}

	return list;
}

bool Game::save(const QString &fileName)
{
	if (!isRunning() || fileName.isEmpty()) {
		return false;
	}

	QFile file( fileName );
	if ( file.open(QIODevice::ReadWrite) )
	{
		QTextStream stream( &file );
		stream << "(;FF[4]GM[1]SZ[" << m_boardSize << "]" << endl;
		stream << "PB[" << m_blackPlayer.name() << "]PW[" << m_whitePlayer.name() << "]" << endl;
		stream << "KM[" << m_komi << "]HA[" << m_fixedHandicap << "]" << endl;

		foreach (const Move &move, getHistory()) {
			stream << ";" << move.toSGF(m_boardSize);
		}

		stream << ")";

		file.close();

		return true;
	}

	//m_process.write("printsgf " + fileName.toLatin1() + '\n');
	//return waitResponse();

	return false;
}

bool Game::setBoardSize(int size)
{
	Q_ASSERT(size >= 1 && size <= 19);
	if (!isRunning()) {
		return false;
	}

	m_process.write("boardsize " + QByteArray::number(size) + '\n');
	if (waitResponse()) {
		// Changing size wipes the board, start again with black player.
		setCurrentPlayer(m_blackPlayer);
		m_boardSize = size;
		m_fixedHandicap = 0;
		m_consecutivePassMoveNumber = 0;
		m_currentMove = 0;
		m_movesList.clear();
		m_undoStack.clear();
		m_analysisMovesList.clear();

		emit boardSizeChanged(size);
		emit boardChanged();
		return true;
	} else {
		return false;
	}
}

bool Game::setKomi(float komi)
{
	Q_ASSERT(komi >= 0);
	if (!isRunning()) {
		return false;
	}

	m_process.write("komi " + QByteArray::number(komi) + '\n');
	if (waitResponse()) {
		m_komi = komi;
		return true;
	} else {
		return false;
	}
}

bool Game::setFixedHandicap(int handicap)
{
	Q_ASSERT(handicap >= 2 && handicap <= 9);
	if (!isRunning()) {
		return false;
	}

	if (handicap <= fixedHandicapUpperBound()) {
		m_process.write("fixed_handicap " + QByteArray::number(handicap) + '\n');
		if (waitResponse()) {
			// Black starts with setting his (fixed) handicap as it's first turn
			// which means, white is next.
			setCurrentPlayer(m_whitePlayer);
			m_fixedHandicap = handicap;

			// Add black moves to the move_list
			foreach (const Stone &stone, stones(m_blackPlayer)) {
				m_movesList.append(Move(&m_blackPlayer, stone));
			}


			emit boardChanged();
			return true;
		} else {
			return false;
		}
	} else {
		//kWarning() << "Handicap" << handicap << " not set, it is too high!";
		return false;
	}
}

int Game::fixedHandicapUpperBound()
{
	switch (m_boardSize) {  // Handcrafted values reflect what GnuGo accepts
		case 7:
		case 8:
		case 10:
		case 12:
		case 14:
		case 16:
		case 18: return 4;
		case 9:
		case 11:
		case 13:
		case 15:
		case 17:
		case 19: return 9;
		default: return 0;
	}
}

bool Game::playMove(const Move &move, bool undoable)
{
	return playMove(*move.player(), move.stone(), undoable);
}

bool Game::playMove(const Player &player, const Stone &stone, bool undoable)
{
	if (!isRunning()) {
		return false;
	}

	if (m_haveToPopAnalysis) {
		popTryMoves();
	}

	const Player *tmp = &player;
	if (!tmp->isValid()) {
		//qDebug() << "Invalid player argument, using current player!";
		tmp = m_currentPlayer;
	}

	if (undoable) {                         // Do undo stuff if desired
		Player *player;
		UndoCommand::MoveType moveType;
		QString undoStr;
		if (tmp->isWhite()) {
			player = &m_whitePlayer;
			if (stone.isValid(m_boardSize)) {
				moveType = UndoCommand::Stone;
				undoStr = tr("White %1", "stone coordinate").arg(stone.toString());
			} else {
				moveType = UndoCommand::Passed;
				undoStr = tr("White passed");
			}
		} else {
			player = &m_blackPlayer;
			if (stone.isValid(m_boardSize)) {
				moveType = UndoCommand::Stone;
				undoStr = tr("Black %1", "stone coordinate").arg(stone.toString());
			} else {
				moveType = UndoCommand::Passed;
				undoStr = tr("Black passed");
			}
		}
		//qDebug() << "Push new undo command" << undoStr;
		m_undoStack.push(new UndoCommand(player, moveType, undoStr));
        return true;
	} else {
		QByteArray msg("play ");                    // The command to be sent
		if (tmp->isWhite()) {
			msg.append("white ");
		} else {
			msg.append("black ");
		}
		if (stone.isValid(m_boardSize)) {
			msg.append(stone.toLatin1() + '\n');
		} else {
			msg.append("pass\n");
		}

		m_process.write(msg);                       // Send command to backend
		if (waitResponse()) {
			if (stone.isValid(m_boardSize)) {                  // Normal move handling
				m_movesList.append(Move(tmp, stone));
				m_consecutivePassMoveNumber = 0;
			} else {                                // And pass move handling
				m_movesList.append(Move(tmp, Stone::Pass));
				emit passMovePlayed(*m_currentPlayer);
				if (m_consecutivePassMoveNumber > 0) {
					emit consecutivePassMovesPlayed(m_consecutivePassMoveNumber);
				}
				m_consecutivePassMoveNumber++;
			}
			m_currentMove++;

			if (tmp->isWhite()) {                   // Determine the next current player
				setCurrentPlayer(m_blackPlayer);
			} else {
				setCurrentPlayer(m_whitePlayer);
			}

			emit boardChanged();
			return true;
		} else {
			return false;
		}
	}
}

bool Game::generateMove(const Player &player, bool undoable)
{
	if (!isRunning()) {
		return false;
	}
	bool boardChange = false;
	const Player *tmp = &player;
	if (!tmp->isValid()) {
		//qDebug() << "Invalid player argument, using current player!";
		tmp = m_currentPlayer;
	}

	if (tmp->isWhite()) {
		m_process.write("level " + QByteArray::number(m_whitePlayer.strength()) + '\n');
		waitResponse(); // Setting level is not mission-critical, no error checking
		m_process.write("genmove white\n");
	} else {
		m_process.write("level " + QByteArray::number(m_blackPlayer.strength()) + '\n');
		waitResponse(); // Setting level is not mission-critical, no error checking
		m_process.write("genmove black\n");
	}
	if (waitResponse(true)) {
		Player *player;
		UndoCommand::MoveType moveType;
		QString undoStr;

		if (tmp->isWhite()) {
			player = &m_whitePlayer;
		} else {
			player = &m_blackPlayer;
		}
		if (m_response == "PASS") {
			m_currentMove++;
			emit passMovePlayed(*m_currentPlayer);
			if (m_consecutivePassMoveNumber > 0) {
				emit consecutivePassMovesPlayed(m_consecutivePassMoveNumber);
				m_gameFinished = true;
			}
			m_consecutivePassMoveNumber++;
			moveType = UndoCommand::Passed;
			if (tmp->isWhite()) {
				undoStr = tr("White passed");
			} else {
				undoStr = tr("Black passed");
			}
		} else if (m_response == "resign") {
			emit resigned(*m_currentPlayer);
			m_gameFinished = true;
			moveType = UndoCommand::Resigned;
			if (tmp->isWhite()) {
				undoStr = tr("White resigned");
			} else {
				undoStr = tr("Black resigned");
			}
		} else {
			m_currentMove++;
			m_movesList.append(Move(tmp, Stone(m_response)));
			m_consecutivePassMoveNumber = 0;
			moveType = UndoCommand::Stone;
			if (tmp->isWhite()) {
				undoStr = tr("White %1", "response from Go engine").arg(m_response);
			} else {
				undoStr = tr("Black %1", "response from Go engine").arg(m_response);
			}
			boardChange = true;
		}

		if (undoable) {
			//qDebug() << "Push new undo command" << undoStr;
			m_undoStack.push(new UndoCommand(player, moveType, undoStr));
		}
		if (tmp->isWhite()) {
			setCurrentPlayer(m_blackPlayer);
		} else {
			setCurrentPlayer(m_whitePlayer);
		}
		if (boardChange) {
			emit boardChanged();
		}
		return true;
	} else {
		return false;
	}
}

bool Game::undoMove()
{
	if (!isRunning()) {
		return false;
	}

	const UndoCommand *undoCmdFrom = static_cast<const UndoCommand*>(m_undoStack.command(m_lastUndoIndex-1));

	if (undoCmdFrom && undoCmdFrom->moveType() == UndoCommand::Analysis) {
		return popTryMoves();
	} else {
		m_process.write("undo\n");
		if (waitResponse()) {
			if (canUndo()) {
				Move lastMove = m_movesList.takeLast();
				m_currentMove--;
				if (lastMove.player()->isComputer()) {
					// Do one more undo
					m_process.write("undo\n");
					if (waitResponse()) {
						lastMove = m_movesList.takeLast();
						m_currentMove--;
					}
				}
				if (lastMove.player()->isWhite()) {
					setCurrentPlayer(m_whitePlayer);
				} else {
					setCurrentPlayer(m_blackPlayer);
				}
				if (m_consecutivePassMoveNumber > 0) {
					m_consecutivePassMoveNumber--;
				}
			}
			const UndoCommand *undoCmdOn = static_cast<const UndoCommand*>(m_undoStack.command(m_lastUndoIndex-2));

			if (undoCmdOn && undoCmdOn->moveType() == UndoCommand::Analysis) {
				// We go to an analysis move, we have to replay all moves
				int number = undoCmdOn->text().split(" ").at(1).toInt();
				m_currentAnalysisMovesList = &m_analysisMovesList[number-1];
				execAnalysis();
			}

			emit boardChanged();
			return true;
		} else {
			return false;
		}
	}
	return true;
}

bool Game::redoMove(int index)
{
	if (!isRunning()) {
		return false;
	}

	if (index == -1)
		index = (m_undoStack.index()-1);

	const UndoCommand *undoCmd = static_cast<const UndoCommand*>(m_undoStack.command(index));

	Player *player = undoCmd->player();
	QString stone = undoCmd->text().split(" ").at(1);

	if (undoCmd->moveType() == UndoCommand::Passed) {
		//qDebug() << "Redo a pass move for" << player << undoCmd->text();
		return playMove(*player, Stone(), false);         // E.g. pass move
	} else if (undoCmd->moveType() == UndoCommand::Resigned) {
		// Note: Altough it is possible to undo after a resign and redo it,
		//       it is a bit questionable whether this makes sense logically.
		//qDebug() << "Redo a resign for" << player << undoCmd->text();
		emit resigned(*player);
		m_gameFinished = true;
		//emit resigned(*m_currentPlayer);
	} else if (undoCmd->moveType() == UndoCommand::Analysis) {
		// We go to an analysis move, we have to replay all moves
		int number = undoCmd->text().split(" ").at(1).toInt();
		m_currentAnalysisMovesList = &m_analysisMovesList[number-1];
		execAnalysis();
	} else {
		//qDebug() << "Redo a normal move for" << player << undoCmd->text();
		return playMove(*player, Stone(stone), false);
	}

	return false;
}

bool Game::popTryMoves()
{
	if (!isRunning()) {
		return false;
	}

	bool result  = true;

	for (int i = 0; i < m_currentAnalysisMovesList->count(); i++) {
		m_process.write("undo\n");
		result = result && waitResponse();
		emit boardChanged();
	}

	m_haveToPopAnalysis = false;
	emit popAnalysisTerritories();
	return result;
}

Move Game::lastMove() const
{
	Q_ASSERT(!m_movesList.isEmpty());
	/*if (m_movesList.isEmpty())
		return Move(m_currentPlayer, Stone());
	else*/
	return m_movesList.last();
}

int Game::moveCount()
{
	if (!isRunning()) {
		return 0;
	}

	m_process.write("move_history\n");          // Query fixed handicap and store it
	if (waitResponse()) {
		return m_response.count('\n') + 1;
	}
	return 0;
}

QList<Stone> Game::stones(const Player &player)
{
	QList<Stone> list;
	if (!isRunning()) {
		return list;
	}

	// Invalid player means all stones
	if (!player.isWhite()) {
		m_process.write("list_stones black\n");
		if (waitResponse() && !m_response.isEmpty()) {
			foreach (const QString &pos, m_response.split(' ')) {
				list.append(Stone(pos));
			}
		}
	}
	if (!player.isBlack()) {
		m_process.write("list_stones white\n");
		if (waitResponse() && !m_response.isEmpty()) {
			foreach (const QString &pos, m_response.split(' ')) {
				list.append(Stone(pos));
			}
		}
	}
	return list;
}

QList<Move> Game::moves(const Player &player)
{
	QList<Move> list;
	if (!isRunning()) {
		return list;
	}

	if (!player.isValid()) {
		list = m_movesList;
	} else {
		foreach (const Move &move, m_movesList) {
			if (move.player()->color() == player.color()) {
				list.append(move);
			}
		}
	}
	return list;
}

QList<Stone> Game::liberties(const Stone &stone)
{
	QList<Stone> list;
	if (!isRunning() || !stone.isValid(m_boardSize)) {
		return list;
	}

	m_process.write("findlib " + stone.toLatin1() + '\n');
	if (waitResponse() && !m_response.isEmpty()) {
		foreach (const QString &entry, m_response.split(' ')) {
			list.append(Stone(entry));
		}
	}
	return list;
}

QList<Stone> Game::bestMoves(const Player &player)
{
	QList<Stone> list;
	if (!isRunning() || !player.isValid()) {
		return list;
	}

	if (player.isWhite()) {
		m_process.write("top_moves_white\n");
	} else {
		m_process.write("top_moves_black\n");
	}
	if (waitResponse(true) && !m_response.isEmpty()) {
		QStringList parts = m_response.split(' ');
		if (parts.size() % 2 == 0) {
			for (int i = 0; i < parts.size(); i += 2) {
				list.append(Stone(parts[i], QString(parts[i + 1]).toFloat()));
			}
		}
	}
	return list;
}

QList<Stone> Game::legalMoves(const Player &player)
{
	QList<Stone> list;
	if (!isRunning() || !player.isValid()) {
		return list;
	}

	if (player.isWhite()) {
		m_process.write("all_legal white\n");
	} else {
		m_process.write("all_legal black\n");
	}
	if (waitResponse() && !m_response.isEmpty()) {
		foreach (const QString &entry, m_response.split(' ')) {
			list.append(Stone(entry));
		}
	}
	return list;
}

void Game::genTerritory (const Stone &stone, Territory &territory, QList<QList<bool> > &checked)
{

	Player::Color color = stoneColor(stone);

	int i = stone.x() > 'I' ? stone.x() - 'A' - 1 : stone.x() - 'A';
	int j = stone.y() - 1;

	if (color == Player::Black) {
		if (territory.isInvalid()) {
			territory.setColor(FinalBlackTerritory);
		} else if (territory.isWhite()) {
			territory.setColor(FinalDead);
		}
	} else if (color == Player::White) {
		if (territory.isInvalid()) {
			territory.setColor(FinalWhiteTerritory);
		} else if (territory.isBlack()) {
			territory.setColor(FinalDead);
		}
	} else if (!checked.at(i).at(j)) {
		territory.addStone(stone);
		checked[i].replace(j, true);
		foreach (Stone s, stone.neighbourhood(m_boardSize)) {
			genTerritory(s, territory, checked);
		}
	}
}

Player::Color Game::stoneColor(const Stone &stone)
{
	if (!isRunning() || !stone.isValid(m_boardSize) || (m_movesList.isEmpty() && m_currentAnalysisMovesList->isEmpty())) {
		return Player::Invalid;
	}

	foreach (Move const move, m_movesList) {
		if (stone == move.stone()) {
			return move.player()->color();
		}
	}

	foreach (Move const move, *m_currentAnalysisMovesList) {
		if (stone == move.stone()) {
			return move.player()->color();
		}
	}

	return Player::Empty;

	/*m_process.write("color " + stone.toLatin1() + "\n");
	if (waitResponse()) {
		if (m_response == "black") return Player::Black;
		else if (m_response == "white") return Player::White;
		else if (m_response == "empty") return Player::Empty;
		else return Player::Invalid;
	} else {
		return Player::Invalid;
	}*/
}

int Game::captures(const Player &player)
{
	if (!isRunning() || !player.isValid()) {
		return 0;
	}

	if (player.isWhite()) {
		m_process.write("captures white\n");
	} else {
		m_process.write("captures black\n");
	}
	return waitResponse() ? m_response.toInt() : 0;
}

Game::FinalState Game::finalState(const Stone &stone)
{
	if (!isRunning() || !stone.isValid(m_boardSize)) {
		return FinalStateInvalid;
	}

	m_process.write("final_status " + stone.toLatin1() + '\n');
	if (waitResponse()) {
		if (m_response == "alive") return FinalAlive;
		else if (m_response == "dead") return FinalDead;
		else if (m_response == "seki") return FinalSeki;
		else if (m_response == "white_territory") return FinalWhiteTerritory;
		else if (m_response == "blacK_territory") return FinalBlackTerritory;
		else if (m_response == "dame") return FinalDame;
		else return FinalStateInvalid;
	} else
		return FinalStateInvalid;
}

QList<Stone> Game::finalStates(FinalState state)
{
	QList<Stone> list;
	if (!isRunning() || state == FinalStateInvalid) {
		return list;
	}

	QByteArray msg("final_status_list ");
	switch (state) {
		case FinalAlive: msg.append("alive"); break;
		case FinalDead: msg.append("dead"); break;
		case FinalSeki: msg.append("seki"); break;
		case FinalWhiteTerritory: msg.append("white_territory"); break;
		case FinalBlackTerritory: msg.append("black_territory"); break;
		case FinalDame: msg.append("dame"); break;
		case FinalStateInvalid: break; // Will never happen
	}
	msg.append('\n');
	m_process.write(msg);
	if (waitResponse() && !m_response.isEmpty()) {
		foreach (const QString &entry, m_response.split(' ')) {
			list.append(Stone(entry));
		}
	}
	return list;
}

QList<Territory> Game::finalAnalysisStates() {

	QList<Territory> list;
	if (!isRunning()) {
		return list;
	}

	QList< QList<bool> > checked;
	for (int i = 0; i < m_boardSize; i++) {
		checked.push_back(QList<bool>());
		for (int j = 0; j < m_boardSize; j++) {
			checked[i].push_back(false);
		}
	}

	Territory territory;
	for (int i = 0; i < m_boardSize; i++) {
		for (int j = 0; j < m_boardSize; j++) {
			territory = Territory();
			if (!checked[i][j]) {
				int x = i >= 8 ? i+1 : i;
				Stone stone = Stone(x + 'A', j + 1);
				if (stoneColor(stone) == Player::Empty) {
					genTerritory(stone, territory, checked);
					territory.genCenter();
				}
			}
			if (territory.isWhite() || territory.isBlack()) {
				list.append(territory);
			}
		}
	}



	/*	QByteArray msg("initial_influence white influence_regions\n");
	m_process.write(msg);
	if (waitResponse() && !m_response.isEmpty()) {
		int i = 0;
		int j = 0;
		foreach (const QString &entry, m_response.split(' ')) {
			QString value = entry;
			if (entry.startsWith("\n")) {
				i++;
				j = 0;
				value = entry.mid(1);
			}
			if (value != "") {
				if (j == 8) {
					j ++;
				}
				if (state == FinalWhiteTerritory && value == "3") {
					list.append(Stone('A' + j, m_boardSize - i));
				} else if (state == FinalBlackTerritory && value == "-3") {
					list.append(Stone('A' + j, m_boardSize - i));
				}
				j++;
			}
		}
	}*/
	return list;
}

Score Game::finalScore()
{
	if (!isRunning()) {
		return Score();
	}

	m_process.write("final_score\n");
	return waitResponse() ? Score(m_response) : Score();
}

Score Game::estimatedScore()
{
	if (!isRunning()) {
		return Score();
	}

	m_process.write("estimate_score\n");
	return waitResponse() ? Score(m_response) : Score();
}

bool Game::waitResponse(bool nonBlocking)
{
	if (m_process.state() != QProcess::Running) {   // No GTP connection means no computing fun!
		switch (m_process.error()) {
			case QProcess::FailedToStart: m_response = "No Go game is running!"; break;
			case QProcess::Crashed: m_response = "The Go game crashed!"; break;
			case QProcess::Timedout: m_response = "The Go game timed out!"; break;
			case QProcess::WriteError: m_response = m_process.readAllStandardError(); break;
			case QProcess::ReadError: m_response = m_process.readAllStandardError(); break;
			case QProcess::UnknownError: m_response = "Unknown error!"; break;
		}
		qWarning() << "Command failed:" << m_response;
		return false;
	}

	if (nonBlocking) {
		emit waiting(true);
	}

	// Wait for finished command execution. We have to do this untill '\n\n' (or '\r\n\r\n' or
	// '\r\r' in case of MS Windows and Mac, respectively) arrives in our input buffer to show
	// that the Go game is done processing our request. The 'nonBlocking' parameter decides
	// whether we block and wait (suitable for short commands) or if we continue processing
	// events in between to stop the UI from blocking (suitable for longer commands).
	// The latter may introduce flickering.
	m_response.clear();
	do {
		if (nonBlocking) {
			while (m_waitAndProcessEvents) {    // The flag is modified by another slot
				qApp->processEvents();          // called when QProcess signals readyRead()
			}
			m_waitAndProcessEvents = true;
		} else {
			m_process.waitForReadyRead();       // Blocking wait
		}
		m_response += m_process.readAllStandardOutput();
	} while (!m_response.endsWith(QLatin1String("\r\r")) &&
			 !m_response.endsWith(QLatin1String("\n\n")) &&
			 !m_response.endsWith(QLatin1String("\r\n\r\n")));

	if (nonBlocking) {
		emit waiting(false);
	}

	if (m_response.size() < 1) {
		return false;
	}
	QChar tmp = m_response[0];                  // First message character indicates success or error
	m_response.remove(0, 2);                    // Remove the first two chars (e.g. "? " or "= ")
	m_response = m_response.trimmed();          // Remove further whitespaces, newlines, ...
	return tmp != '?';                          // '?' Means the game didn't understand the query
}

void Game::gameSetup()
{
	emit boardInitialized();
}

void Game::readyRead()
{
	m_waitAndProcessEvents = false;
}

void Game::undoIndexChanged(int index)
{
	int diff = index - m_lastUndoIndex;

	// Bigger index changed means that the QUndoView got a MouseClickEvent.
	// This means we have to replay more undo/redo steps than normally.
	// If the difference is greater than 1 we have to replay diff-1 steps.
	if (diff > 0) {
		redoMove(m_lastUndoIndex);
		m_lastUndoIndex++;
		if (diff > 1) {
			undoIndexChanged(index);
		}
	} else if (diff < 0) {
		undoMove();
		m_lastUndoIndex--;
		if (diff < -1) {
			undoIndexChanged(index);
		}
	}
}

void Game::setCurrentPlayer(Player &player)
{
	m_currentPlayer = &player;
	emit currentPlayerChanged(*m_currentPlayer);
}

void Game::setAnalysisMode(bool analyze)
{
	if (analyze) {
		m_backUpPlayer = m_currentPlayer;

		const UndoCommand *undoCmd = static_cast<const UndoCommand*>(m_undoStack.command(m_undoStack.index()-1));

		if (undoCmd && undoCmd->moveType() == UndoCommand::Analysis) {
			int number = undoCmd->text().split(" ").at(1).toInt();
			m_currentAnalysisMovesList = &m_analysisMovesList[number-1];
			m_undoStack.undo();
		} else {
			m_analysisMovesList.append(QList<Move>());
			m_currentAnalysisMovesList = &m_analysisMovesList.last();
		}
	} else if (m_inAnalysis) {
		setCurrentPlayer(*m_backUpPlayer);
		if (!m_haveToPopAnalysis)
			m_analysisMovesList.pop_back();
	}

	m_inAnalysis = analyze;
}

bool Game::isAnalysisStone(const Stone &stone)
{
    if (!m_haveToPopAnalysis)
        return false;

	bool result = false;
	int i = 0;

	while (i < m_currentAnalysisMovesList->count() && !result) {
		result = (stone == m_currentAnalysisMovesList->at(i).stone());
		i++;
	}
	return result;
}

bool Game::playAnalysisMove(const Move &move, bool undoable)
{
	return playAnalysisMove(*move.player(), move.stone(), undoable);
}

bool Game::playAnalysisMove(const Player &player, const Stone &stone, bool undoable)
{
	if (!isRunning()) {
		return false;
	}

	const Player *tmp = &player;
	if (!tmp->isValid()) {
		tmp = m_currentPlayer;
	}

	if (undoable) {                         // Do undo stuff if desired
		Player *player;
		if (tmp->isWhite()) {
			player = &m_whitePlayer;
		} else {
			player = &m_blackPlayer;
		}
		if (stone.isValid(m_boardSize)) {
			m_currentAnalysisMovesList->append(Move(player, stone));
		}
	} else {
		//When the Launch Analysis button is pressed
		QByteArray msg("play ");                    // The command to be sent
		if (tmp->isWhite()) {
			msg.append("white ");
		} else {
			msg.append("black ");
		}
		if (stone.isValid(m_boardSize)) {
			msg.append(stone.toLatin1() + '\n');
		}

		m_process.write(msg);                       // Send command to backend
		if (waitResponse()) {
			// Don't know why I done that, it works without, but let it just in case ...
			/*m_movesList.append(Move(tmp, stone));
			m_consecutivePassMoveNumber = 0;
			m_currentMove++;*/

			emit boardChanged();
		} else {
			return false;
		}
	}

	emit analysisChanged();
	return true;
}

bool Game::removeAnalysisMove(const Stone &stone) {

	if (!isRunning()) {
		return false;
	}

	bool result = false;
	int i = 0;
	while (i < m_currentAnalysisMovesList->count() && !result) {
		result = stone == m_currentAnalysisMovesList->at(i).stone();
		if (result) {
			m_currentAnalysisMovesList->removeAt(i);
			emit analysisChanged();
		}
		i++;
	}
	return result;
}


bool Game::launchAnalysis()
{
	if (!isRunning()) {
		return false;
	}

	//Add the analysis line in the undo stack
	QString undostr = "Analysis " + QString::number( m_analysisMovesList.count() );
	m_undoStack.push(new UndoCommand(m_currentPlayer, UndoCommand::Analysis, undostr));

	emit boardChanged();
	return true;
}

void Game::execAnalysis() {
	qDebug() << m_currentAnalysisMovesList->count();
	foreach(const Move &move, *m_currentAnalysisMovesList) {
		playAnalysisMove(move, false);
	}

	m_haveToPopAnalysis = true;

	emit analysisDone();
	//getInfluenceGrid();
	emit boardChanged();
}

void Game::getInfluenceGrid() {
	/*	QByteArray msg("initial_influence white influence_regions\n");    // The command to be sent
	m_process.write(msg);
	if (waitResponse()) {
		int influences[m_boardSize][m_boardSize];
		int i = 0;
		int j = 0;

	// 4 white stone
	// 3 white territory
	// 2 white moyo
	// 1 white area
	// 0 neutral
	//-1 black area
	//-2 black moyo
	//-3 black territory
	//-4 black stone
		foreach (const QString &response, m_response.split(" ")) {
			if(response == "\n") {
				i++;
				j=0;
			} else {
				if (response == "-3") {
					influences[i][j] = -1;
				} else if (response == "3") {
					influences[i][j] = 1;
				} else {
					influences[i][j] = 0;
				}
				j++;
			}
		}

		QList<Stone> stones = stones(new Player());
		QList < QList <*Stone> > areas;
		bool onArea = false;
		int last = 0;

		for (int i = 0; i < m_boardSize; i++) {
			for (int j = 'A'; j < 'A' + m_boardSize; j++) {
				if (!onArea && (influences[i][j] == -1 || influences[i][j] == 1)) {
					onArea = true;
					areas.last().push_back(&stones.at(stones.indexOf(Stone(i, j))));
				} else {
					if ()
				}
			}
		}
	}*/
}
