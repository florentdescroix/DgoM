/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "territory.h"

Territory::Territory()
	: m_color(Game::FinalStateInvalid), m_stonesList(QList<Stone>()), m_value(0)
{
}

Territory::Territory(const Game::FinalState color, const QList<Stone> stones)
	: m_color(color), m_stonesList(stones), m_value(stones.count())
{
}

void Territory::addStone(Stone stone)
{
	m_stonesList.append(stone);
	m_value ++;
}

void Territory::removeStone(Stone stone)
{
	m_stonesList.removeAll(stone);
	m_value --;
}

void Territory::genCenter()
{
	int x = 0;
	int y = 0;
	foreach (const Stone &stone, m_stonesList) {
		x = x + stone.x();
		y = y + stone.y();
	}

	x = x / m_value;
	y = y / m_value;

	if (x == 'I')
		x ++;

	Stone stone(x, y);

	/** If the center is not in the list, we take the nearest **/
	float dist = 0;
	float min = -1;
	if (!m_stonesList.contains(stone)) {
		Stone tmp = stone;
		foreach (const Stone &other, m_stonesList) {
			dist = qSqrt(
					   qPow(other.x() - stone.x(), 2) +
					   qPow(other.y() - stone.y(), 2));

			if (dist < min || min == -1) {
				min = dist;
				tmp = other;
			}
		}
		stone = tmp;
	}

	m_center = stone;
}
