/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <ui_mainwindow.h>

#include <QDir>
#include <QMainWindow>
#include <QTranslator>
#include <QToolBar>

class QDockWidget;
class QUndoView;
class GeneralConfig;

class Game;
class GameScene;
class GameView;
class Player;
class SetupWidget;
class AnalysisWidget;

/**
 * The MainWindow class acts as the main window for the DgoM graphical
 * user interface.
 *
 * @author Sascha Peilicke <sasch.pe@gmx.de>
 * @since 0.1
 */
class MainWindow : public QMainWindow, private Ui::MainWindow
{
		Q_OBJECT

	public:
		explicit MainWindow(const QString &fileName = "", QWidget *parent = 0);
		~MainWindow();

	private slots:
		void slotLanguageChanged(QAction* action);
		void setUp();							///< Set up board, game, docks ...
		void newGame();                         ///< Configure new game
		void loadGame();                        ///< Configure loaded game
		bool loadGame(const QString &fileName);
		void backendError();                    ///<
		void saveGame();                        ///< Save current game state
		void startGame();                       ///< React on start button
		void finishGame();                      ///< Final screen, scores, ...
		void finishAnalysis();					///< Show zones and scores for the analysis
		void undo();                            ///< Undo last move
		void redo();                            ///< Redo last move
		void pass();                            ///< Pass current move
		void hint();                            ///< Show a playing hint
		void showPreferences();                 ///< Show configuration dialog
		void applyPreferences();                ///< React on changed config
		void showBusy(bool busy);               ///< Signal a busy app
		void showFinishGameAction();
		void showAnalysisTerritory();
		void playerChanged();
		void generateMove();
		void passMovePlayed(const Player &);
		void analyzeGame(bool analyze);

	protected:

	private:
		// this event is called, when a new translator is loaded or the system language is changed
		void changeEvent(QEvent*);
		// make all the connection between UI and code
		void initMenu();

		// loads a language by the given language shortcur (e.g. de, en)
		void loadLanguage(const QString& rLanguage);

		// creates the language menu dynamically from the content of m_langPath
		void createLanguageMenu(void);

        Ui::MainWindow *ui; // ui definition from designer
		QTranslator m_translator; // contains the translations for this application
		QTranslator m_translatorQt; // contains the translations for qt
		QString m_currLang; // contains the currently loaded language
		QString m_langPath; // Path of language files. This is always fixed to /languages.

		void setupActions();
		void setupDockWindows();

		bool isBackendWorking();

		Game *m_game;                           ///< Handles complete game state
		GameScene *m_gameScene;                 ///< QGraphicsScene for Go board
		GameView *m_gameView;                   ///< QGraphicsView for Go board

		GeneralConfig *m_generalConfig;			///< Configuration windows

		SetupWidget *m_setupWidget;             ///< Part of dock widget
		QUndoView *m_undoView;                  ///< Part of dock widget
		AnalysisWidget *m_analysisWidget;       ///< Part of dock widget

		QDockWidget *m_setupDock;               ///< Game setup dock widget
		QDockWidget *m_gameDock;                ///< Game info dock widget
		QDockWidget *m_movesDock;               ///< Move history dock widget
		QDockWidget *m_errorDock;               ///< Dock shown when errors occur
		QDockWidget *m_analysisDock;            ///< Game analysis dock widget

		QToolBar *m_moveToolBar;
		QToolBar *m_viewToolBar;
};

#endif // MAINWINDOW_H
