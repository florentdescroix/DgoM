/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"

#include "../game/game.h"
#include "../game/score.h"
#include "../game/territory.h"
#include "config/generalconfig.h"
#include "graphicsview/gamescene.h"
#include "graphicsview/gameview.h"
#include "graphicsview/themerenderer.h"
#include "widgets/errorwidget.h"
#include "widgets/gamewidget.h"
#include "widgets/setupwidget.h"
#include "widgets/analysiswidget.h"
#include "src/preferences.h"
#include "src/include/configdialog.h"

#include <QDockWidget>
#include <QTimer>
#include <QUndoView>
#include <QFileDialog>

MainWindow::MainWindow(const QString &fileName, QWidget *parent)
    : QMainWindow(parent)
	, m_game(new Game(this))
	, m_gameScene(new GameScene(m_game))
	, m_gameView(new GameView(m_gameScene))
    , m_generalConfig(new GeneralConfig())
	, m_moveToolBar(new QToolBar())
    , m_viewToolBar(new QToolBar())
{
	setupUi(this);

	setWindowIcon(QIcon(Preferences::applicationPath() + "/data/icons/hi128-app-kigo.png"));

	setCentralWidget(m_gameView);

	createLanguageMenu();
	setupActions();
	setupDockWindows();

	connect(m_game, SIGNAL(waiting(bool)), this, SLOT(showBusy(bool)));
	connect(m_game, SIGNAL(consecutivePassMovesPlayed(int)), this, SLOT(showFinishGameAction()));
	connect(m_game, SIGNAL(resigned(Player)), this, SLOT(finishGame()));
	connect(m_game, SIGNAL(passMovePlayed(Player)), this, SLOT(passMovePlayed(Player)));
	connect(m_game, SIGNAL(popAnalysisTerritories()), this, SLOT(showAnalysisTerritory()));

	if (isBackendWorking()) {
		if (!loadGame(fileName)) {
			newGame();
		}
	} else {
		backendError();
	}
}


MainWindow::~MainWindow()
{
	delete ui;
}

// we create the menu entries dynamically, dependent on the existing translations.
void MainWindow::createLanguageMenu(void)
{
	QActionGroup* langGroup = new QActionGroup(menuLanguage);
	langGroup->setExclusive(true);

	connect(langGroup, SIGNAL (triggered(QAction *)), this, SLOT (slotLanguageChanged(QAction *)));

	// format systems language
	QString defaultLocale = QLocale::system().name(); // e.g. "de_DE"
	defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "de"

	QString langPath = Preferences::applicationPath() + "/data/languages";
	QDir dir(langPath);

	QStringList fileNames = dir.entryList(QStringList("Translation_*.qm"));

	for (int i = 0; i < fileNames.size(); ++i) {
		// get locale extracted by filename
		QString locale;
		locale = fileNames[i]; // "Translation_de.qm"
		locale.truncate(locale.lastIndexOf('.')); // "Translation_de"
		locale.remove(0, locale.indexOf('_') + 1); // "de"

		QString lang = QLocale::languageToString(QLocale(locale).language());
		QIcon ico(QString("%1/flags/%2.png").arg(langPath).arg(locale));

		QAction *action = new QAction(ico, lang, this);
		action->setCheckable(true);
		action->setData(locale);

		menuLanguage->addAction(action);
		langGroup->addAction(action);

		// set default translators and language checked
		if (defaultLocale == locale)
		{
			action->setChecked(true);
		}
	}
}

// Called every time, when a menu entry of the language menu is called
void MainWindow::slotLanguageChanged(QAction* action)
{
	if(0 != action) {
		// load the language dependant on the action content
		loadLanguage(action->data().toString());
	}
}

void switchTranslator(QTranslator& translator, const QString& filename)
{
	// remove the old translator
	qApp->removeTranslator(&translator);

	// load the new translator
	QString langPath = Preferences::applicationPath() + "/data/languages";
	if(translator.load(langPath + "/" + filename))
		qApp->installTranslator(&translator);
}

void MainWindow::loadLanguage(const QString& rLanguage)
{
	if(m_currLang != rLanguage) {
		m_currLang = rLanguage;
		QLocale locale = QLocale(m_currLang);
		QLocale::setDefault(locale);
		QString languageName = QLocale::languageToString(locale.language());
		switchTranslator(m_translator, QString("Translation_%1.qm").arg(rLanguage));
		switchTranslator(m_translatorQt, QString("qt_%1.qm").arg(rLanguage));
		Ui::MainWindow::statusBar->showMessage(tr("Current Language changed to %1").arg(languageName));
	}
}

void MainWindow::changeEvent(QEvent* event)
{
	if(0 != event) {
		if (event->type() == QEvent::LanguageChange)
			retranslateUi(this);
		// this event is send, if the system, language changes
		else if (event->type() == QEvent::LocaleChange)
		{
			QString locale = QLocale::system().name();
			locale.truncate(locale.lastIndexOf('_'));
			loadLanguage(locale);
		}
	}

	QMainWindow::changeEvent(event);
}

void MainWindow::setUp()
{
	m_game->start(Preferences::engineCommand());

	m_gameScene->showTerritory(false);
	m_gameScene->showAnalysisTerritory(false);
	m_gameView->setInteractive(false);

	actionNew_game->setEnabled(true);
	actionLoad_game->setEnabled(true);
	actionSave->setEnabled(false);
	actionStart_game->setEnabled(true);
	actionFinish_game->setEnabled(false);

	actionUndo->setEnabled(false);
	actionRedo->setEnabled(false);
	actionPass->setEnabled(false);
	actionHint->setEnabled(false);
	actionAnalyze_Game->setEnabled(false);
	if (actionAnalyze_Game->isChecked()) {
		actionAnalyze_Game->setChecked(false);
	}

	disconnect(m_game, SIGNAL(canRedoChanged(bool)), actionRedo, SLOT(setEnabled(bool)));
	disconnect(m_game, SIGNAL(canUndoChanged(bool)), actionUndo, SLOT(setEnabled(bool)));
	disconnect(m_game, SIGNAL(currentPlayerChanged(Player)), this, SLOT(playerChanged()));

	m_gameDock->setVisible(false);
	m_gameDock->toggleViewAction()->setEnabled(false);
	m_movesDock->setVisible(false);
	m_movesDock->toggleViewAction()->setEnabled(false);
	m_analysisDock->setVisible(false);
	m_setupDock->setVisible(true);
	m_errorDock->setVisible(false);
}

void MainWindow::newGame()
{
	setUp();

	m_setupWidget->newGame();
	m_gameScene->showMessage(tr("Set up a new game..."));
}

void MainWindow::loadGame()
{
	QString fileName = QFileDialog::getOpenFileName(this,
													tr("Open File"),
													QApplication::applicationDirPath()+"/data/games/",
													tr("SGF Files (*.sgf)"));
	if (!fileName.isEmpty()) {
		loadGame(fileName);
	}
}

bool MainWindow::loadGame(const QString &fileName)
{
	if (!fileName.isEmpty() && QFile(fileName).exists()) {
		setUp();

		m_setupWidget->loadedGame(fileName);
		m_gameScene->showMessage(tr("Set up a loaded game..."));
		return true;
	} else {
		m_gameScene->showMessage(tr("Unable to load game..."));
		//Note: New game implied here
		return false;
	}
}

void MainWindow::backendError()
{
	m_gameView->setInteractive(false);

	actionNew_game->setEnabled(false);
	actionLoad_game->setEnabled(false);
	actionSave->setEnabled(false);
	actionStart_game->setEnabled(false);
	actionFinish_game->setEnabled(false);

	actionUndo->setEnabled(false);
	actionRedo->setEnabled(false);
	actionPass->setEnabled(false);
	actionHint->setEnabled(false);
	actionAnalyze_Game->setEnabled(false);

	disconnect(m_game, SIGNAL(canRedoChanged(bool)), actionRedo, SLOT(setEnabled(bool)));
	disconnect(m_game, SIGNAL(canUndoChanged(bool)), actionUndo, SLOT(setEnabled(bool)));
	disconnect(m_game, SIGNAL(currentPlayerChanged(Player)), this, SLOT(playerChanged()));
	m_gameDock->setVisible(false);
	m_gameDock->toggleViewAction()->setEnabled(false);
	m_movesDock->setVisible(false);
	m_movesDock->toggleViewAction()->setEnabled(false);
	m_analysisDock->setVisible(false);
	m_setupDock->setVisible(false);
	m_errorDock->setVisible(true);

	analyzeGame(false);
}

void MainWindow::saveGame()
{
	QString fileName = QFileDialog::getSaveFileName(this,
													tr("Save File"),
													QDir::homePath(),
													tr("SGF Files (*.sgf)"));

	if (!fileName.isEmpty()) {
		if (m_game->save(fileName))
			m_gameScene->showMessage(tr("Game saved..."));
		else
			m_gameScene->showMessage(tr("Unable to save game."));
	}
}

void MainWindow::startGame()
{
	actionSave->setEnabled(true);
	actionStart_game->setEnabled(false);
	actionFinish_game->setEnabled(false);

	m_setupWidget->commit();

	//Decide on players how to display the UI
	if (m_game->whitePlayer().isHuman() && m_game->blackPlayer().isHuman()) {
		connect(m_game, SIGNAL(canRedoChanged(bool)), actionRedo, SLOT(setEnabled(bool)));
		connect(m_game, SIGNAL(canUndoChanged(bool)), actionUndo, SLOT(setEnabled(bool)));

		actionPass->setEnabled(true);
		actionHint->setEnabled(true);

		m_gameView->setInteractive(true);
		m_undoView->setEnabled(true);

		m_gameScene->showPlacementMarker(true);

		actionAnalyze_Game->setEnabled(true);
	} else {
		actionPass->setEnabled(false);
		actionHint->setEnabled(false);

		m_gameView->setInteractive(false);
		m_undoView->setEnabled(false);

		m_gameScene->showPlacementMarker(false);

		actionAnalyze_Game->setEnabled(false);
	}

	connect(m_game, SIGNAL(currentPlayerChanged(Player)), this, SLOT(playerChanged()));
	connect(m_game, SIGNAL(analysisDone()), this, SLOT(finishAnalysis()));
	// Trigger the slot once to make a move if the starting player
	// (black) is a computer player.
	playerChanged();

	m_setupDock->setVisible(false);
	m_errorDock->setVisible(false);
	m_gameDock->setVisible(true);
	m_gameDock->toggleViewAction()->setEnabled(true);
	m_movesDock->setVisible(true);
	m_movesDock->toggleViewAction()->setEnabled(true);
	m_analysisDock->setVisible(false);

	m_gameScene->showMessage(tr("Game started..."));
}

void MainWindow::finishGame()
{
	m_gameView->setInteractive(false);
	m_gameScene->showHint(false);
	m_gameScene->showAnalysis(false);
	m_gameScene->showAnalysisTerritory(false);
	m_gameScene->showTerritory(true);

	actionUndo->setEnabled(false);
	actionRedo->setEnabled(false);
	actionPass->setEnabled(false);
	actionHint->setEnabled(false);
	actionStart_game->setEnabled(false);
	actionFinish_game->setEnabled(false);
	actionAnalyze_Game->setEnabled(false);

	m_undoView->setEnabled(false);

	// kDebug() << "Fetching final score from engine ...";
	Score score = m_game->estimatedScore();
	QString name;
	if (score.color() == 'W') {
		name = m_game->whitePlayer().name() + " (White)";
	} else {
		name = m_game->blackPlayer().name() + " (Black)";
	}
	// Show a score message that stays visible until the next
	// popup message arrives
	if (score.upperBound() == score.lowerBound()) {
		m_gameScene->showMessage(tr("%1 won with a score of %2.").arg(name).arg(score.score()), 0);
	} else {
		m_gameScene->showMessage(tr("%1 won with a score of %2 (bounds: %3 and %4).").arg(name).arg(score.score()).arg(score.upperBound()).arg(score.lowerBound()), 0);
	}
}

void MainWindow::finishAnalysis()
{
	m_gameScene->showAnalysisTerritory(true);
	m_analysisDock->setVisible(false);

	int whiteScore = 0;
	int blackScore = 0;

	foreach (const Territory &territory, m_game->finalAnalysisStates()) {
		if (territory.isWhite()) {
			whiteScore += territory.value();
		} else if (territory.isBlack()) {
			blackScore += territory.value();
		}
	}

	QString nameW = m_game->whitePlayer().name() + " (White)";
	QString nameB = m_game->blackPlayer().name() + " (Black)";
	// Show a score message that stays visible until the next
	// popup message arrives
	m_gameScene->showMessage(tr("%1 got %2 territories and %3 got %4 territories").arg(nameW).arg(whiteScore).arg(nameB).arg(blackScore), 1000);
}

void MainWindow::undo()
{
	if (m_game->isRunning()) {
		m_game->undoStack()->undo();
		m_gameScene->showMessage(tr("Undone move"));
		m_gameScene->showHint(false);
	}
}

void MainWindow::redo()
{
	if (m_game->isRunning()) {
		m_game->undoStack()->redo();
		m_gameScene->showMessage(tr("Redone move"));
		m_gameScene->showHint(false);
	}
}

void MainWindow::pass()
{
	if (m_game->playMove(m_game->currentPlayer())) {     // E.g. Pass move
		//m_gameScene->showMessage(i18n("Passed move"));
		m_gameScene->showHint(false);
	}
}

void MainWindow::hint()
{
	m_gameScene->showHint(true);
	//m_gameScene->showMessage(i18n("These are the recommended moves..."));
}

void MainWindow::showPreferences()
{
	m_generalConfig->show();
}

void MainWindow::applyPreferences()
{
	// kDebug() << "Update settings based on changed configuration...";
	m_gameScene->showLabels(Preferences::showBoardLabels());

	ThemeRenderer::self()->loadTheme(Preferences::theme());

	if (!isBackendWorking()) {
		backendError();
	} else if (m_game->engineCommand() != Preferences::engineCommand()) {
		// Restart the Go game if the game command was changed by the user.
		m_gameScene->showMessage(tr("Backend was changed, restart necessary..."));
		newGame();
	}
}

void MainWindow::showBusy(bool busy)
{
	//Decide on players how to display the UI
	if (m_game->whitePlayer().isHuman() || m_game->blackPlayer().isHuman()) {
		if (busy) {
			actionUndo->setDisabled(true);
			actionRedo->setDisabled(true);
		} else {
			// Only re-enable undo/redo if it actually makes sense
			if (m_game->canUndo()) {
				actionUndo->setDisabled(false);
			}
			if (m_game->canRedo()) {
				actionRedo->setDisabled(false);
			}
		}
		actionPass->setDisabled(busy);
		actionHint->setDisabled(busy);
		m_undoView->setEnabled(false);
	}

	m_gameView->setInteractive(!busy);
}

void MainWindow::showFinishGameAction()
{
	actionFinish_game->setEnabled(true);
}

void MainWindow::showAnalysisTerritory()
{
	m_gameScene->showAnalysisTerritory(false);
}

void MainWindow::playerChanged()
{
	if (!m_game->currentPlayer().isHuman() && !m_game->isFinished()) {
        QTimer::singleShot(200, this, SLOT(generateMove()));
	}
}

void MainWindow::generateMove()
{
	m_game->generateMove(m_game->currentPlayer());
}

void MainWindow::passMovePlayed(const Player &player)
{
	if (player.isComputer()) {
		if (player.isWhite()) {
			m_gameScene->showMessage(tr("White passed"));
		} else {
			m_gameScene->showMessage(tr("Black passed"));
		}
	}
}

void MainWindow::setupActions()
{
	// Game menu
	connect (actionNew_game,			SIGNAL(triggered(bool)),	this,						SLOT(newGame()));
	connect (actionLoad_game,			SIGNAL(triggered(bool)),	this,						SLOT(loadGame()));
	connect (actionSave,				SIGNAL(triggered(bool)),	this,						SLOT(saveGame()));
	connect (actionQuit,				SIGNAL(triggered(bool)),	QApplication::instance(),	SLOT(quit()));
	connect (actionStart_game,			SIGNAL(triggered(bool)),	this,						SLOT(startGame()));
	connect (actionFinish_game,			SIGNAL(triggered(bool)),	this,						SLOT(finishGame()));

	mainToolBar->addAction(actionNew_game);
	mainToolBar->addAction(actionStart_game);
	mainToolBar->addAction(actionFinish_game);

	connect (actionMain_Toolbar,		SIGNAL(toggled(bool)),		mainToolBar,				SLOT(setVisible(bool)));

	// Move menu
	connect (actionUndo,				SIGNAL(triggered(bool)),	this,						SLOT(undo()));
	connect (actionRedo,				SIGNAL(triggered(bool)),	this,						SLOT(redo()));
	connect (actionPass,				SIGNAL(triggered(bool)),	this,						SLOT(pass()));

	m_moveToolBar->addAction(actionUndo);
	m_moveToolBar->addAction(actionRedo);
	m_moveToolBar->addAction(actionPass);

	connect (actionMove_Toolbar,		SIGNAL(toggled(bool)),		m_moveToolBar,				SLOT(setVisible(bool)));

	addToolBar(m_moveToolBar);

	// View menu
	connect (actionHint,				SIGNAL(triggered(bool)),	this,						SLOT(hint()));
	connect (actionShow_move_number,	SIGNAL(toggled(bool)),		m_gameScene,				SLOT(showMoveNumbers(bool)));
	connect (actionAnalyze_Game,		SIGNAL(toggled(bool)),		this,						SLOT(analyzeGame(bool)));

	actionShow_move_number->setChecked(Preferences::showMoveNumbers());

	m_viewToolBar->addAction(actionHint);
	m_viewToolBar->addAction(actionShow_move_number);
	m_viewToolBar->addAction(actionAnalyze_Game);
	addToolBar(m_viewToolBar);

	connect (actionView_Toolbar,		SIGNAL(toggled(bool)),		m_viewToolBar,				SLOT(setVisible(bool)));

	// Settings menu
	connect (actionConfiguration,		SIGNAL(triggered(bool)),	this,						SLOT(showPreferences()));
	connect (m_generalConfig,			SIGNAL(configurationDone()),this,						SLOT(applyPreferences()));
}

void MainWindow::setupDockWindows()
{
	// Setup dock
    m_setupDock = new QDockWidget(tr("Game Setup"), this);
	m_setupDock->setObjectName( QLatin1String("setupDock" ));
	m_setupDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	m_setupWidget = new SetupWidget(m_game, this);
	m_setupDock->setWidget(m_setupWidget);
	connect(m_setupWidget, SIGNAL(startClicked()), this, SLOT(startGame()));
	//m_setupDock->toggleViewAction()->setText(i18nc("@title:window", "Game setup"));
	//m_setupDock->toggleViewAction()->setShortcut(Qt::Key_S);
	//actionCollection->addAction( QLatin1String( "show_setup_panel" ), m_setupDock->toggleViewAction());
	addDockWidget(Qt::RightDockWidgetArea, m_setupDock);

	// Game dock
	m_gameDock = new QDockWidget(tr("Information"), this);
	m_gameDock->setObjectName( QLatin1String("gameDock" ));
	m_gameDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	GameWidget *gameWidget = new GameWidget(m_game, this);
	connect(gameWidget, SIGNAL(finishClicked()), this, SLOT(finishGame()));
	m_gameDock->setWidget(gameWidget);
	m_gameDock->toggleViewAction()->setText(tr("Information"));
	menuDockers->addAction(m_gameDock->toggleViewAction());
	addDockWidget(Qt::RightDockWidgetArea, m_gameDock);

	// Move history dock
	m_movesDock = new QDockWidget(tr("Moves"), this);
	m_movesDock->setObjectName( QLatin1String("movesDock" ));
	m_undoView = new QUndoView(m_game->undoStack());
	m_undoView->setEmptyLabel(tr("No move"));
	m_undoView->setAlternatingRowColors(true);
	m_undoView->setFocusPolicy(Qt::NoFocus);
	m_undoView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	m_undoView->setSelectionMode(QAbstractItemView::NoSelection);
	m_movesDock->setWidget(m_undoView);
	m_movesDock->toggleViewAction()->setText(tr("Moves"));
	menuDockers->addAction(m_movesDock->toggleViewAction());
	addDockWidget(Qt::RightDockWidgetArea, m_movesDock);

	// analysis game dock
	m_analysisDock = new QDockWidget(tr("Analyze"), this);
	m_analysisDock->setObjectName(QLatin1String("analysisDock"));
	m_analysisDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	m_analysisDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	m_analysisWidget = new AnalysisWidget(m_game, m_gameScene, this);
	m_analysisDock->setWidget(m_analysisWidget);
	connect( m_analysisWidget, SIGNAL(leaveAnalysisClicked()), actionAnalyze_Game, SLOT(toggle()));

	addDockWidget(Qt::LeftDockWidgetArea, m_analysisDock);


	m_errorDock = new QDockWidget(tr("Error"), this);
	m_errorDock->setObjectName( QLatin1String("errorDock" ));
	m_errorDock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	ErrorWidget *errorWidget = new ErrorWidget(this);
	connect(errorWidget, SIGNAL(configureClicked()), this, SLOT(showPreferences()));
	m_errorDock->setWidget(errorWidget);
	//m_errorDock->toggleViewAction()->setText(i18nc("@title:window", "Error"));
	//m_errorDock->toggleViewAction()->setShortcut(Qt::Key_E);
	//actionCollection->addAction( QLatin1String( "show_error_panel" ), m_errorDock->toggleViewAction());
	addDockWidget(Qt::BottomDockWidgetArea, m_errorDock);
}

bool MainWindow::isBackendWorking()
{
	Game game;
	return game.start(Preferences::engineCommand());
}

void MainWindow::analyzeGame(bool analyze)
{
	m_analysisDock->setVisible(analyze);
	m_game->setAnalysisMode(analyze);
	m_gameScene->showAnalysis(analyze);

	if (analyze) {
		m_analysisWidget->init();
		m_undoView->setEnabled(false);
		actionUndo->setEnabled(false);
		actionRedo->setEnabled(false);
	} else {
		m_undoView->setEnabled(true);
		actionUndo->setEnabled(true);
		actionRedo->setEnabled(true);
	}
}

