/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "generalconfig.h"
#include "src/game/game.h"
#include "src/preferences.h"

#include <QUrl>
#include <QDir>
#include <QFileInfo>
#include <QTableWidgetItem>
#include <QFileDialog>
#include <QSettings>

GeneralConfig::GeneralConfig(QWidget *parent)
	: QWidget(parent)
{
	setupUi(this);

	loadConfig();

	connect(engineExecutable,	SIGNAL(clicked(bool)),			this,	SLOT(onEngineExecutableClick()));
	connect(engineParameters,	SIGNAL(textChanged(QString)),	this,	SLOT(updateEngineCommand()));
	connect(pushButtonOk,		SIGNAL(clicked(bool)),			this,	SLOT(applyConfig()));
	connect(pushButtonOk,		SIGNAL(clicked(bool)),			this,	SLOT(hide()));
	connect(pushButtonApply,	SIGNAL(clicked(bool)),			this,	SLOT(applyConfig()));
	connect(pushButtonCancel,	SIGNAL(clicked(bool)),			this,	SLOT(hide()));
	connect(themesList,			SIGNAL(clicked(QModelIndex)),	this,	SLOT(onThemeListClick()));

	updateEngineCommand();
}

void GeneralConfig::updateEngineCommand()
{
    QString path = engineExecutablePath->text();
    QString engineCommand = QDir::toNativeSeparators(path) + ' ' + engineParameters->text();

	// Check if the configured Go engine backend actually works and tell the user
	// If engineParameters->text() == "-" all freeze ... no idea why
	Game game;
	if (engineParameters->text() != "-" && game.start(engineCommand)) {
		engineLed->setChecked(true);
	} else {
		engineLed->setChecked(false);
	}
}

void GeneralConfig::onEngineExecutableClick()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"));
    engineExecutablePath->setText(fileName);
}

void GeneralConfig::onThemeListClick()
{
	QSettings *details = new QSettings(Preferences::themePath() + themesList->currentItem()->text().toLower() + ".desktop", QSettings::IniFormat);
	details->beginGroup("KGameTheme");

	QString themePicturePath = Preferences::themePath() + details->value("Preview").toString();
	if (QFileInfo(themePicturePath).exists()) {
		QPixmap pixmap = QPixmap(themePicturePath);
		pixmap = pixmap.scaledToWidth(200);
		themePicture->setPixmap(pixmap);
	}
	authorLabel->setText(details->value("Author").toString());
	contactLabel->setText(details->value("AuthorEmail").toString());
	descriptionLabel->setText(details->value("Description").toString());
}

void GeneralConfig::loadConfig() {

	QDir themeDir = QDir(Preferences::themePath());
	int i = 0;
	foreach (QFileInfo const fileInfo, themeDir.entryInfoList()) {
		if (fileInfo.fileName().endsWith(".desktop")) {
			QTableWidgetItem *item = new QTableWidgetItem();
			themesList->insertRow(i);
			themesList->setItem(i, 0, item);
			//item->setText(fileInfo.fileName().section('.', 0, 0));

			QSettings *details = new QSettings(Preferences::themePath() + fileInfo.fileName(), QSettings::IniFormat);
			details->beginGroup("KGameTheme");
			item->setText(details->value("Name").toString());
			item->setData(1, details->value("FileName"));

			if (Preferences::theme() == item->data(1).toString()) {
				themesList->setCurrentItem(item);
				onThemeListClick();
			}

			i++;
		}
	}

	QString engineCommand = Preferences::engineCommand();
	engineExecutablePath->setText(engineCommand.section(' ', 0, 0));
	engineParameters->setText(engineCommand.section(' ', 1));

	kcfg_ShowBoardLabels->setChecked(Preferences::showBoardLabels());
	kcfg_HintVisibleTime->setValue(Preferences::hintVisibleTime());

	kcfg_HintVisibleTime->setSuffix(tr(" Second(s)", 0, kcfg_HintVisibleTime->value()));
}

void GeneralConfig::applyConfig()
{
	Preferences::setEngineCommand(engineExecutablePath->text() + " " + engineParameters->text());
	Preferences::setEngineWorking(engineLed->isChecked());
	Preferences::setShowBoardLabels(kcfg_ShowBoardLabels->isChecked());
	Preferences::setHintVisibleTime(kcfg_HintVisibleTime->value());
	Preferences::setTheme(themesList->currentItem()->data(1).toString());

	emit configurationDone();
}

#include "moc_generalconfig.cpp"
