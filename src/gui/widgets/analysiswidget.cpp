/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "analysiswidget.h"
#include "src/game/game.h"
#include "../graphicsview/gamescene.h"
#include "src/preferences.h"

#include "../graphicsview/themerenderer.h"

AnalysisWidget::AnalysisWidget(Game *game, GameScene *gameScene, QWidget *parent)
	: QWidget(parent), m_game(game), m_gameScene(gameScene)
{
	Q_ASSERT(m_game);
    setupUi(this);

	QPixmap whiteStone = ThemeRenderer::self()->renderElement(ThemeRenderer::WhiteStone, QSize(48, 48));
    whiteStoneImageLabel->setPixmap(whiteStone);
	QPixmap blackStone = ThemeRenderer::self()->renderElement(ThemeRenderer::BlackStone, QSize(48, 48));
    blackStoneImageLabel->setPixmap(blackStone);
	QPixmap addOne = ThemeRenderer::self()->renderElement(ThemeRenderer::AddOne, QSize(48, 48));
    addOnePawnImageLabel->setPixmap(addOne);
	QPixmap addMultiple = ThemeRenderer::self()->renderElement(ThemeRenderer::AddMultiple, QSize(48, 48));
    addMultiplePawnsImageLabel->setPixmap(addMultiple);
	QPixmap remove = ThemeRenderer::self()->renderElement(ThemeRenderer::Remove, QSize(48, 48));
    removePawnImageLabel->setPixmap(remove);

	connect(launchAnalysisButton, SIGNAL(clicked()), this, SLOT(launchAnalysis()));
    connect(leaveAnalysisButton, SIGNAL(clicked()), this, SIGNAL(leaveAnalysisClicked()));
    connect(whitePlayer, SIGNAL(clicked(bool)), this, SLOT(playerChanged()));
    connect(blackPlayer, SIGNAL(clicked(bool)), this, SLOT(playerChanged()));
    connect(addOnePawn, SIGNAL(clicked(bool)), this, SLOT(analysisToolChanged()));
    connect(addMultiplePawns, SIGNAL(clicked(bool)), this, SLOT(analysisToolChanged()));
    connect(removePawn, SIGNAL(clicked(bool)), this, SLOT(analysisToolChanged()));
}

AnalysisWidget::~AnalysisWidget()
{

}

void AnalysisWidget::init()
{
    if (m_game->currentPlayer().isBlack()) {
        blackPlayer->setChecked(true);
    } else if (m_game->currentPlayer().isWhite()) {
        whitePlayer->setChecked(true);
    }

    if(m_gameScene->analysisTool() == GameScene::AddOnePawn) {
        addOnePawn->setChecked(true);
    } else if (m_gameScene->analysisTool() == GameScene::AddMultiplePawns) {
        addMultiplePawns->setChecked(true);
    } else if (m_gameScene->analysisTool() == GameScene::RemovePawn) {
        removePawn->setChecked(true);
    }
}

void AnalysisWidget::playerChanged()
{
    if(whitePlayer->isChecked()) {
        m_game->setCurrentPlayer(m_game->whitePlayer());
    } else if (blackPlayer->isChecked()) {
        m_game->setCurrentPlayer(m_game->blackPlayer());
    }

}

void AnalysisWidget::analysisToolChanged()
{
    if(addOnePawn->isChecked()) {
		m_gameScene->setAnalysisTool(GameScene::AddOnePawn);
	} else if (addMultiplePawns->isChecked()) {
		m_gameScene->setAnalysisTool(GameScene::AddMultiplePawns);
    } else if (removePawn->isChecked()) {
		m_gameScene->setAnalysisTool(GameScene::RemovePawn);
	}
}

void AnalysisWidget::launchAnalysis()
{
	m_game->launchAnalysis();
	emit leaveAnalysisClicked();
}

#include "moc_analysiswidget.cpp"
