/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gamewidget.h"
#include "src/game/game.h"
#include "src/game/stone.h"
#include "src/preferences.h"

#include "../graphicsview/themerenderer.h"

#include <QDebug>

GameWidget::GameWidget(Game *game, QWidget *parent)
	: QWidget(parent), m_game(game)
{
	Q_ASSERT(m_game);
	setupUi(this);

	finishButton->setIcon(QIcon( QLatin1String( "media-playback-stop" )));

	connect(m_game, SIGNAL(boardInitialized()), this, SLOT(init()));
	connect(m_game, SIGNAL(boardChanged()), this, SLOT(update()));
	connect(m_game, SIGNAL(consecutivePassMovesPlayed(int)), this, SLOT(enableFinishButton()));
	connect(finishButton, SIGNAL(clicked()), this, SLOT(finishButtonClicked()));
}

void GameWidget::init()
{
	if (!m_game->isRunning()) {
		qDebug() << "Game is not running, no information update";
		return;
	}

	if (Preferences::whitePlayerHuman()) {
		whiteNameLabel->setText(Preferences::whitePlayerName());
	} else {
		whiteNameLabel->setText(tr("Computer (level %1)").arg(Preferences::whitePlayerStrength()));
	}
	if (Preferences::blackPlayerHuman()) {
		blackNameLabel->setText(Preferences::blackPlayerName());
	} else {
		blackNameLabel->setText(tr("Computer (level %1)").arg(Preferences::blackPlayerStrength()));
	}
	komiLabel->setText(QString::number(m_game->komi()));
	handicapLabel->setText(tr("%1 Stone(s)", 0, m_game->fixedHandicap()).arg(m_game->fixedHandicap()));

	QPixmap whiteStone = ThemeRenderer::self()->renderElement(ThemeRenderer::WhiteStone, QSize(48, 48));
	whiteStoneImageLabel->setPixmap(whiteStone);
	QPixmap blackStone = ThemeRenderer::self()->renderElement(ThemeRenderer::BlackStone, QSize(48, 48));
	blackStoneImageLabel->setPixmap(blackStone);

    finishButton->setEnabled(false);

    update();
}

void GameWidget::update()
{
	moveLabel->setText(QString::number(m_game->currentMoveNumber()));

	if (m_game->moves().size() > 0) {
		Move last = m_game->lastMove();
		if (last.player()->isWhite()) {
			lastMoveLabel->setText(tr("%1 (white)", "Indication who played the last move").arg(last.stone().toString()));
		} else if (last.player()->isBlack()) {
			lastMoveLabel->setText(tr("%1 (black)", "Indication who played the last move").arg(last.stone().toString()));
		} else {
			lastMoveLabel->clear();
		}
	}

	if (m_game->currentPlayer().isWhite()) {
		QPixmap whiteStone = ThemeRenderer::self()->renderElement(ThemeRenderer::WhiteStone, QSize(64, 64));
		currentPlayerImageLabel->setPixmap(whiteStone);
		currentPlayerLabel->setText(tr("White's turn"));
	} else if (m_game->currentPlayer().isBlack()) {
		QPixmap blackStone = ThemeRenderer::self()->renderElement(ThemeRenderer::BlackStone, QSize(64, 64));
		currentPlayerImageLabel->setPixmap(blackStone);
		currentPlayerLabel->setText(tr("Black's turn"));
	} else {
		currentPlayerLabel->clear();
	}

	whiteCapturesLabel->setText(tr("%1 capture(s)", 0, m_game->captures(m_game->whitePlayer())).arg(m_game->captures(m_game->whitePlayer())));
	blackCapturesLabel->setText(tr("%1 capture(s)", 0, m_game->captures(m_game->blackPlayer())).arg(m_game->captures(m_game->blackPlayer())));
}

void GameWidget::finishButtonClicked()
{
	finishButton->setEnabled(false);
	emit finishClicked();
}

void GameWidget::enableFinishButton()
{
	finishButton->setEnabled(true);
}

#include "moc_gamewidget.cpp"

