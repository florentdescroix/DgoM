/*
	Copyright 2015 by Florent Descroix <florentdescroix@gmail.com>
				  and Damien  Moulard   <dam.moulard@gmail.com>

	Original Copyright 2008 Sascha Peilicke <sasch.pe@gmx.de>
	For Kigo software

	This file is part of DgoM

	DgoM is free software: you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License or (at your option) version 3 or any later version
	accepted by the membership of KDE e.V. (or its successor approved
	by the membership of KDE e.V.), which shall act as a proxy
	defined in Section 14 of version 3 of the license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QSettings>
#include <QDebug>
#include <QString>

class Preferences : public QSettings
{
	public:

		static Preferences *self();
		~Preferences();

		/**
	  Set The current game engine command with (optional) parameters
	*/
		static
		void setEngineCommand( const QString & v )
		{
			self()->setValue("EngineCommand", v);
			self()->mEngineCommand = v;
		}

		/**
	  Get The current game engine command with (optional) parameters
	*/
		static
		QString engineCommand()
		{
			return self()->mEngineCommand;
		}

		/**
	  Set EngineWorking
	*/
		static
		void setEngineWorking( bool v )
		{
			self()->setValue("EngineWorking", v);
			self()->mEngineWorking = v;
		}

		/**
	  Get EngineWorking
	*/
		static
		bool engineWorking()
		{
			return self()->mEngineWorking;
		}

		/**
	  Set The graphical theme to be used
	*/
		static
		void setTheme( const QString & v )
		{
			self()->setValue("Theme", v);
			self()->mTheme = v;
		}

		/**
	  Get The graphical theme to be used
	*/
		static
		QString theme()
		{
			return self()->mTheme;
		}

		/**
	  Set Determines whether board labels are shown
	*/
		static
		void setShowBoardLabels( bool v )
		{
			self()->setValue("ShowBoardLabels", v);
			self()->mShowBoardLabels = v;
		}

		/**
	  Get Determines whether board labels are shown
	*/
		static
		bool showBoardLabels()
		{
			return self()->mShowBoardLabels;
		}

		/**
	  Set Move numbers are drawn onto stones if enabled
	*/
		static
		void setShowMoveNumbers( bool v )
		{
			self()->setValue("ShowMoveNumbers", v);
			self()->mShowMoveNumbers = v;
		}

		/**
	  Get Move numbers are drawn onto stones if enabled
	*/
		static
		bool showMoveNumbers()
		{
			return self()->mShowMoveNumbers;
		}

		/**
	  Set Number of seconds for which a hint is visible
	*/
		static
		void setHintVisibleTime( double v )
		{
			if (v < 1.0)
			{
				qDebug() <<"setHintVisibleTime: self()->value" << v <<" is less than the minimum self()->value of 1.0";
				v = 1.0;
			}

			if (v > 5.0)
			{
				qDebug() <<"setHintVisibleTime: self()->value" << v <<" is greater than the maximum self()->value of 5.0";
				v = 5.0;
			}

			self()->setValue("HintVisibleTime", v);
			self()->mHintVisibleTime = v;
		}

		/**
	  Get Number of seconds for which a hint is visible
	*/
		static
		double hintVisibleTime()
		{
			return self()->mHintVisibleTime;
		}

		/**
	  Set Is black a human player?
	*/
		static
		void setBlackPlayerHuman( bool v )
		{
			self()->setValue("BlackPlayerHuman", v);
			self()->mBlackPlayerHuman = v;
		}

		/**
	  Get Is black a human player?
	*/
		static
		bool blackPlayerHuman()
		{
			return self()->mBlackPlayerHuman;
		}

		/**
	  Set The name of the black player
	*/
		static
		void setBlackPlayerName( const QString & v )
		{
			self()->setValue("BlackPlayerName", v);
			self()->mBlackPlayerName = v;
		}

		/**
	  Get The name of the black player
	*/
		static
		QString blackPlayerName()
		{
			return self()->mBlackPlayerName;
		}

		/**
	  Set The strength of the black player
	*/
		static
		void setBlackPlayerStrength( int v )
		{
			if (v < 1)
			{
				qDebug() <<"setBlackPlayerStrength: self()->value" << v <<" is less than the minimum self()->value of 1";
				v = 1;
			}

			if (v > 10)
			{
				qDebug() <<"setBlackPlayerStrength: self()->value" << v <<" is greater than the maximum self()->value of 10";
				v = 10;
			}

			self()->setValue("BlackPlayerStrength", v);
			self()->mBlackPlayerStrength = v;
		}

		/**
	  Get The strength of the black player
	*/
		static
		int blackPlayerStrength()
		{
			return self()->mBlackPlayerStrength;
		}

		/**
	  Set Is white a human player?
	*/
		static
		void setWhitePlayerHuman( bool v )
		{
			self()->setValue("WhitePlayerHuman", v);
			self()->mWhitePlayerHuman = v;
		}

		/**
	  Get Is white a human player?
	*/
		static
		bool whitePlayerHuman()
		{
			return self()->mWhitePlayerHuman;
		}

		/**
	  Set The name of the white player
	*/
		static
		void setWhitePlayerName( const QString & v )
		{
			self()->setValue("WhitePlayerName", v);
			self()->mWhitePlayerName = v;
		}

		/**
	  Get The name of the white player
	*/
		static
		QString whitePlayerName()
		{
			return self()->mWhitePlayerName;
		}

		/**
	  Set The strength of the white player
	*/
		static
		void setWhitePlayerStrength( int v )
		{
			if (v < 1)
			{
				qDebug() <<"setWhitePlayerStrength: self()->value" << v <<" is less than the minimum self()->value of 1";
				v = 1;
			}

			if (v > 10)
			{
				qDebug() <<"setWhitePlayerStrength: self()->value" << v <<" is greater than the maximum self()->value of 10";
				v = 10;
			}

			self()->setValue("WhitePlayerStrength", v);
			self()->mWhitePlayerStrength = v;
		}

		/**
	  Get The strength of the white player
	*/
		static
		int whitePlayerStrength()
		{
			return self()->mWhitePlayerStrength;
		}

		/**
	  Set Go board size
	*/
		static
		void setBoardSize( int v )
		{
			if (v < 3)
			{
				qDebug() <<"setBoardSize: self()->value" << v <<" is less than the minimum self()->value of 3";
				v = 3;
			}

			if (v > 19)
			{
				qDebug() <<"setBoardSize: self()->value" << v <<" is greater than the maximum self()->value of 19";
				v = 19;
			}

			self()->setValue("BoardSize", v);
			self()->mBoardSize = v;
		}

		/**
	  Get Go board size
	*/
		static
		int boardSize()
		{
			return self()->mBoardSize;
		}

		/**
	  Set Komi
	*/
		static
		void setKomi( double v )
		{
			if (v < 0)
			{
				qDebug() <<"setKomi: self()->value" << v <<" is less than the minimum self()->value of 0";
				v = 0;
			}

			if (v > 50)
			{
				qDebug() <<"setKomi: self()->value" << v <<" is greater than the maximum self()->value of 50";
				v = 50;
			}

			self()->setValue("Komi", v);
			self()->mKomi = v;
		}

		/**
	  Get Komi
	*/
		static
		double komi()
		{
			return self()->mKomi;
		}

		/**
	  Set Extra stones for the black player
	*/
		static
		void setFixedHandicapValue( int v )
		{
			if (v < 2)
			{
				qDebug() <<"setFixedHandicapValue: self()->value" << v <<" is less than the minimum self()->value of 2";
				v = 2;
			}

			if (v > 9)
			{
				qDebug() <<"setFixedHandicapValue: self()->value" << v <<" is greater than the maximum self()->value of 9";
				v = 9;
			}

			self()->setValue("FixedHandicapValue", v);
			self()->mFixedHandicapValue = v;
		}

		/**
	  Get Extra stones for the black player
	*/
		static
		int fixedHandicapValue()
		{
			return self()->self()->mFixedHandicapValue;
		}

		/**
	  Set The path where datas are
	*/
		static
		void setApplicationPath( const QString & v )
		{
			self()->mApplicationPath = v;
		}

		/**
	  Get The path where datas are
	*/
		static
		QString applicationPath()
		{
			return self()->mApplicationPath;
		}

		/**
	  Set The path where themes are
	*/
		static
		void setThemePath( const QString & v )
		{
			self()->mThemePath = v;
		}

		/**
	  Get The path where themes are
	*/
		static
		QString themePath()
		{
			return self()->mThemePath;
		}

	private:
		Preferences();
		Preferences(Preferences const&);
		void operator=(Preferences const&);

		// Backend
		QString mEngineCommand;
		bool mEngineWorking;

		// UserInterface
		QString mTheme;
		bool mShowBoardLabels;
		bool mShowMoveNumbers;
		double mHintVisibleTime;

		// Game
		bool mBlackPlayerHuman;
		QString mBlackPlayerName;
		int mBlackPlayerStrength;
		bool mWhitePlayerHuman;
		QString mWhitePlayerName;
		int mWhitePlayerStrength;
		int mBoardSize;
		double mKomi;
		int mFixedHandicapValue;

		// Data
		QString mApplicationPath;
		QString mThemePath;
};
#endif

