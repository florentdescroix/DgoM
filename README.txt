[Project]
DgoM.pro is a Qt Creator project file
We used Qt 5.4.1 
You can download Qt here : https://www.qt.io/download/ or use you package manager

[Launch]
DgoM needs GnuGo, available for Windows, in the data/ directory
If you are not using Windows, you'll have to configure DgoM for using your own GnuGo executable
DgoM needs other things found in the data/ directory and the file settings_default.ini
To launch it after a build, make sure to copy the data/ directory and the settings_default.ini file into your build directory

[Create a standalone executable]
If you want to build a standalone Qt application, you have to build statically your project
To do so, you have to build statically Qt before building your project
Please follow this tutorial : https://zahidhasan.wordpress.com/2014/09/28/how-to-install-statically-built-qt-64-bit-with-mingw-on-windows-8-1/

[Install]
Like for launching, the data/ directory and the settings_default.ini file have to be in the executable directory